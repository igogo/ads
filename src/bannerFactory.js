/**
 * Created by Ihor Skliar <skliar.ihor@gmail.com> on 8/4/2016.
 */

import Banner from './banners/Banner'
import BannerAndTarget from './banners/BannerAndTarget'
import BannerScript from './banners/BannerScript'


const bannerFactory = (opt, el) => {
    switch (opt.action) {
        case 'banner': {
            return new Banner(opt, el)
            break
        }
        case 'banner&target': {
            return new BannerAndTarget(opt, el)
            break
        }
        case 'script': {
            return new BannerScript(opt, el)
            break
        }
    }
}

export default bannerFactory