/**
 * Created by Ihor Skliar <skliar.ihor@gmail.com> on 8/4/2016.
 */

import AbstractBanner from './abstract/AbstractBanner'

export default class BannerScript extends AbstractBanner {
    get html() {
        return this.opt.body
    }
}