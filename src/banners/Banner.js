/**
 * Created by Ihor Skliar <skliar.ihor@gmail.com> on 8/4/2016.
 */

import AbstractBanner from './abstract/AbstractBanner'

export default class Banner extends AbstractBanner {
    get html() {
        return `
            <a href=${this.opt.url} target="_blank">
                <img src=${this.opt.src} width=${this.opt.style.width} height=${this.opt.style.height} />
            </a>
        `
    }
}