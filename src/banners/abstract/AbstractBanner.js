/**
 * Created by Ihor Skliar <skliar.ihor@gmail.com> on 8/4/2016.
 */


export default class AbstractBanner {
    constructor(opt, el) {
        if (this.constructor === AbstractBanner) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }

        this.el = el
        this.opt = opt

        if (this.html === undefined) {
            throw new TypeError("Must override method getHtml");
        }

        this.render()
    }

    get html() {
        return undefined
    }

    render() {
        document.write(typeof this.html === 'string' ? this.html : this.html.outerHTML)
    }
}