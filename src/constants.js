/**
 * Created by Ihor Skliar <skliar.ihor@gmail.com> on 8/4/2016.
 */
export default {
    url: '/json/get-banner',


    APPLE_VENDOR_NAME: 'Apple Computer, Inc.',
    GOOGLE_VENDOR_NAME: 'Google Inc.',
}