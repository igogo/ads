/**
 * Created by Ihor Skliar <skliar.ihor@gmail.com> on 8/6/16.
 */

import CONST from './constants'

const isUaFake = () => {
    switch (navigator.vendor) {
        case CONST.APPLE_VENDOR_NAME: {
            return /(windows)/.test(navigator.userAgent.toLowerCase())
            break
        }
        case CONST.GOOGLE_VENDOR_NAME: {
            return /(iphone|ipad|windwos)/.test(navigator.userAgent.toLowerCase())
            break
        }

        /**
         * @TODO add more checks
         */
    }

    return false
}

const isPhantomJS = () => {
    if (
        /PhantomJS/.test(window.navigator.userAgent) ||
        window.callPhantom ||
        window._phantom
    ) {
        return true
    }

    return false
}


export default () => {
    const plugins = []

    for (let i = 0; i < navigator.plugins.length; i++) {
        plugins.push(navigator.plugins[i].name)
    }

    const info = {
        referrer: document.referrer,
        location: window.location.href,
        plugins: plugins,
        isUaFake: isUaFake(),
        isPhantomJS: isPhantomJS()
    }

    return info
}