/**
 * Created by Ihor Skliar <skliar.ihor@gmail.com> on 8/6/16.
 */


export default (msg, file, line, col, error) => {
    alert(`error: ${file} (${line}, ${col}) (${msg}) (${error})`)
}