/**
 * Created by Ihor Skliar <skliar.ihor@gmail.com> on 8/4/2016.
 */

import 'es5-shim'
import 'promise-polyfill-browserify'
import 'whatwg-fetch'
import qs from 'qs'
import assign from 'object-assign'
import CONST from './constants'
import bannerFactory from './bannerFactory'
import errorHandler from './errorHandler'
import clientInfo from './clientInfo'

(function() {
    const scripts = document.getElementsByTagName( 'script' )
    const currentScriptTag = scripts[ scripts.length - 1 ]

    const width = currentScriptTag.getAttribute('data-width')
    const height = currentScriptTag.getAttribute('data-height')
    const pid = currentScriptTag.getAttribute('data-pid')

    const data = assign({
        pid: pid,
        width: width,
        height: height
    }, clientInfo())

    fetch(CONST.url + '?' + qs.stringify(data), {
        method: 'GET'
    }).then(res => {
        return res.json()
    }).then(json => {
        bannerFactory(assign(json, {
            style: {
                width: width + 'px',
                height: height + 'px'
            }
        }), currentScriptTag)
    }).catch(ex => {
        console.error('Error', ex)
    })
})()


window.onerror = window.error = errorHandler