'use strict';

var path = require('path');
var data = require(path.normalize(__dirname + '/json/data.json'));
var express = require('express');
var app = express();

app.use(express.static('dest'));

app.get('/json/get-banner', function (req, res) {
    const index = Math.round(Math.random() * (data.length - 1));
    res.send(data[index]);
});

app.get('/', function(req, res) {
    res.sendfile(path.normalize(__dirname + '/index.html'));
});

app.listen(process.env.PORT || 9090);
