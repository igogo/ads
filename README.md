Написание кода выдачи рекламных материалов на сайтах, где этот код размещен, а также в других подобных кодах. Так же детект параметров браузера юзера
Язык написания-JS
Примеры подобных скриптов других систем:
1.
```html
<script type='text/javascript'>var adParams = {p: '0000024', size: '728x90' };</script><script type='text/javascript' src='http://bidingads.com/in/show/?ap=1317'></script>
```
2.
```html
<script type='text/javascript' src='http://twerkmediartb.com/ad/nginad.js?pzoneid=8&height=90&width=728&tld=testacc.com&cb=1465312169'></script>
```
3.
```html
<!-- Placement: supply 2 main, Size: 728x90, Type: Banner -->
<!--Copy and paste the code below into the location on your website where the ad will appear.-->
<script type='text/javascript'>
var adParams = {p: '74662067', size: '728x90', serverdomain: 'twerkmedia'   };
</script>
<script type='text/javascript' src='http://cdn.twerkmedia.com/twerkmedia/tags/xbanner/xbanner.js?ap=1300'></script>
```
4.
```html
<!--Copy and paste the code below into the location on your website where the ad will appear.-->
<iframe src='http://cdn.twerkmedia.com/twerkmedia/tags/xdirect/xdirect.html?p=74662067&serverdomain=twerkmedia&size=728x90&ct=html&ap=1300' height='90' width='728' frameborder='0' border='0' marginwidth='0' marginheight='0' scrolling='no' sandbox='allow-forms allow-popups allow-scripts'></iframe>
```
5.
```html
<script data-cfasync=false src="//s.ato.mx/p.js#id=12166&size=300x250"></script>
```

=======================================================================

На выходе мы должны получить нечто подобное, в скрипте 2 входных переменные(берем первый пример). Первая переменная p: '0000024' -это id пользователя(передается далее на сервер для того,чтобы понимать
от какого пользователя пришел тот или иной показ ).Вторая size: '728x90'-размер баннера(передается далее на сервер, для того, чтобы понимать, какого размера баннер на том или ином сайте).
Этот код вставляется на сайты вебмастера,либо в теги к другим сетям(отображаться он будет внутри тегов из примеров в таком случае)


JS код помимо переменных выше должен определять и  отправлять на сервер следующую информацию в переменных - reffer(текущий рефер пользователя), location(страница, где данный баннер размещен),детект основных плагинов(flash,java,silverlight),детект подмены юзерагента,детект ботов(основанных на phantomjs и подобных)
Эти данные мы отправляем на сервер при помощи GET запроса, сервер отвечает скрипту в JSON формате. Возможные ответы:

1. {"action": "banner", "src": "Ссылка на картинку для отображения", "url": "Ссылка для клика по баннеру(куда попадает юзер при клике)"}
В этом случае мы получаем данные и выводим баннер на сайте или внутри тега

2. {"action": "banner&target", "src": "Ссылка на картинку для отображения", "url": "Ссылка для клика по баннеру(куда попадает юзер при клике)", "target": "Ссылка для редиректа, закодированная в base64", "timer": "Таймер редиректа"}
В этом случае мы также выводим баннер, но через время в секундах, указанное в переменной timer, мы осуществляем редирект пользователя на ссылку, указанную в target. Обратите внимание, что этот код должен осуществлять редирект как непосредственно из сайта вебмастера, 
так и внутри других тегов(примеры выше)

3. {"action": "script", "body": "JS код другой рекламной сети"}
В этом случае мы в своем теге должны отобразить чужой тег(как пример теги выше) и показать рекламу

Других ответов сервер слать не может.

Скрипт должнен работать кроссбраузерно, работать на десктоп/мобильных девайсах.
Так же, если скрипт запрашивается по протоколу http-запрос на сервер должен быть в http. Если же https -то по https. Обращение должно происходить к тому же домену, в папку /json в корне
Функция windows.top.location или ее аналог для редиректа должна быть закодирована
Код должен иметь модульную структуру для внедрения новых детектов или логики работы. Код не должен содержать зависимостей типа Jquery

Готовый код должен выводить баннер на любых устройствах, любых версиях браузеров и ос